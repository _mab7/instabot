<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reddit extends Model
{
    protected $guarded = [];
    public $timestamps = null;
    public $table = "reddit";
}
