<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use Goutte;
use Carbon\Carbon;
use App\Reddit;

class HomeController extends Controller
{

    private $cookieJar;
    private $client;

    public function __construct(){
        $this->cookieJar = new \GuzzleHttp\Cookie\CookieJar();
        $this->client = new Client();
        $this->client->post("https://app.alphafire.me/login",[
            'form_params' => [
                'username' => 'xxxxxxxxxxxxxx@gmail.com',
                'password' => 'xxxxxxxxxxxxxx',
                'action' => 'login'
            ],
            'cookies' =>$this->cookieJar,
        ]);
    }
    

     public function sendRequest(){
        ini_set('max_execution_time', 0);
        set_time_limit(0);
        try {
            $date = Carbon::now();
            $nbrTimeToPublish = 20;
// 't3_922hjp' , t3_923zhs
                $posts = $this->reddit('t3_922hjp');
                foreach($posts as $post) {
                    //check if post already posted
                    if(Reddit::where('image_id', $post['image_id'])->first()) {
                        dump('Already posted');
                        continue;
                    }
                    //generate date to publish
                    if($nbrTimeToPublish == 0) {
                        $date->addDay();
                        $nbrTimeToPublish = 20;
                    }

                    //Upload image to the host , and get image data
                    $image_data = $this->uploadFromUrl($post['image_link']);
                    dump($image_data);
                    if(isset($image_data) &&  isset($image_data->data)){
                        $img_id = $image_data->data->file->id;
                        $this->schedule($img_id,$post['tags'],$this->timeToPublish($date));
                        //store the image ID
                        Reddit::create(["image_id" => $post['image_id']]);
                        dump('POST ==> SUCCESS');
                    }

                
                    //decrement     
                    $nbrTimeToPublish--;
                
                }
        }
        catch(Exception $e) {
            dump($e->getMessage());
        }
    }

    public function schedule($img_id,$tags,$timeToPublish){
       $response = $this->client->post("https://app.alphafire.me/post",[
            "form_params" => [
                'action' =>  'post',
                'type' =>  'timeline',
                'media_ids' =>  $img_id,
                'remove_media' =>  '1',
                'caption' =>  $this->caption(),
                'account' =>  '152',
                'is_scheduled' =>  '1',
                'schedule_date' =>  $timeToPublish,
                'user_datetime_format' =>  'Y-m-d H:i',
                'location_label' =>  'Los Angeles, California',
            ],
            'cookies' =>$this->cookieJar,
        ]);

        dump($response->getBody()->getContents());

    }

    //after_id: id of the post from which the scraping will start

    public function reddit($after_id){

        $result = $this->client->request('GET', 'https://gateway.reddit.com/desktopapi/v1/subreddits/memes?rtj=debug&redditWebClient=web2x&app=web2x-client-production&allow_over18=&after='.$after_id.'&dist=14&layout=card&sort=hot',
        [
            'headers' => [
                'User-Agent' => 'testing/1.0',
                'Accept'     => 'application/json',
                'X-Foo'      => ['Bar', 'Baz']
            ]
        ]);

        $data = json_decode($result->getBody()->getContents(),true);
        $post = null;

        foreach($data['posts'] as $key => $value) {

            //get image link
            if(isset($value['preview']['url'])) $post[$key]['image_link'] = $value['preview']['url'];
            else continue;
            //get image ID
            $post[$key]['image_id'] = $key;

            //no tags
            $post[$key]['tags'] = '';


        }

        return $post;
    }

     public function uploadFromUrl($url){
        $response =  $this->client->post("https://app.alphafire.me/file-manager/connector", [
            'multipart' => [
                [
                    'name'     => 'cmd',
                    'contents' => "upload",
                ],
                [
                    'name'     => 'type',
                    'contents' => "url",
                ],
                [
                    'name'     => 'file',
                    'contents' => $url
                ],
            ],
            'cookies' => $this->cookieJar
        ]);
        return json_decode($response->getBody()->getContents());
     }


     public function removeAttahcments(){
        $response = $this->client->post("https://app.alphafire.me/file-manager/connector",[
            "form_params" => [
                'cmd' =>  'retrieve',
                'last_retrieved' =>  0,
                'limit' =>  200,

            ],
            'cookies' =>$this->cookieJar,
        ]);

        $files = json_decode($response->getBody()->getContents())->data->files;
        foreach($files as $file) {
             $remove = $this->client->post("https://app.alphafire.me/file-manager/connector",[
                "form_params" => [
                    'cmd' =>  'remove',
                    'id' =>  $file->id,
                ],
                'cookies' =>$this->cookieJar,
            ]);
            
            dump(json_decode($remove->getBody()->getContents()));
        }

       
    
    }


     public function uploadFromFile(){
          // echo '<pre>';
        // $response =  $client->post("https://app.alphafire.me/file-manager/connector?callback=jQuery31107050655283717486_1532471569444", [

        //     'multipart' => [
        //         [
        //             'name'     => 'cmd',
        //             'contents' => "upload"            ,
        //         ],
        //         [
        //             'name'     => 'type',
        //             'contents' => "file"            ,
        //         ],
        //         [
        //             'name'     => 'file',
        //             'filename' => "sdds.jpg",
        //             'Mime-Type'=> "image/jpeg"            ,
        //             'Content-Type'=> "image/jpeg"            ,
        //             'contents' => fopen("s.jpg","r"),
        //         ],
        //     ],
        //     'cookies' => $cookieJar
        // ]);
        // echo $response->getBody()->getContents();
        // echo '</pre>';
     }

     public function caption(){
         $captions[] = "#memes #dankmemes #fortnite #shitpost #filthyfrank #pewdiepie #cancer #lol #funny #hilarious #edgymemes #gamingmemes #memelord #memegod #animememes #bestmemes #wwememes #nflmemes #offensivememes #dailymemes";

       $captions[] = "#memes #dankmemes #pettymemes #hoodmemes #instamemes #csgomemes #kpopmemes #leagueoflegendsmemes #nbamemes #memevideos #sportsmemes #spongebobmemes #fitnessmemes #dankmeme";


         $caption[]= "#dankmemes #despacito2 #shitpost #cringe #dank #memes #shpitpost #memesdaily #only2genders #despacito #fortnite #fortnitememes #trending ";
    
       $caption[]= "#dankmemes #edgymemes#trends #edgy #trendy #anime #yourmomgay #offensivememes #like4follow #gay #oof #funnymemes #funny #cringememes #daily #6ix9ine #oofmemes #zucc #memes";

         $caption[]= "#memes #memesdaily #hitler #dankmemes #roblox #stolen #nein #fortnite #fortniteapp #furries #yiff #ecchihentai #anime #bigtiddygothgf #bigtiddyanimegirl";


         $caption[]= " #familyguymemes #juul #dispicableme  #spongebobmemes #caprisun #autisum #autisticmemes #robloxgfx #csgo #dank #420 #lol #funny #garfield #memes #meme";

         //return random caption
         return $captions[rand(0, count($captions) - 1)];
     }

     public function timeToPublish($date){

        $min = 1; // 1 AM;
        $max = 23; // 23 PM;

        //PUBLISH 6 TIME A DAY
         return $date->setTime(rand($min,$max), rand(1,60) , rand(1,60))->format('Y-m-d H:i');
     }
}

